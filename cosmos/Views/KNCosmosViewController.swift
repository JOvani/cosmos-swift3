//
//  KNCosmosViewController.swift
//  cosmos
//
//  Created by Jovani Luna on 22/02/17.
//  Copyright © 2017 knotion. All rights reserved.
//

import UIKit
import AVFoundation

enum ActivityCellSize: String{
    case small = "activityCellS"
    case medium = "activityCellM"
    case large = "activityCellL"
}

struct ActivityCellProperties{
    var activityCellSize: ActivityCellSize
    var positionY: CGFloat
    var cellIdentifier: String{
        get{
            return self.activityCellSize.rawValue
        }
    }
}

class KNCosmosViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var randomPosition = ActivityCellPosition.top
    private var cellsProperties: [IndexPath:ActivityCellProperties] = [IndexPath:ActivityCellProperties]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView!.delegate = self
        self.randomPosition = Int.randomNumber(min: 1, max: 2) == 1 ? ActivityCellPosition.top : ActivityCellPosition.bottom
        //self.collectionView.collectionViewLayout.item
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let collectionView = self.collectionView{
            var index = 0
            collectionView.visibleCells.forEach({ (cell) in

                if let activityCell = cell as? ActivityNormalCell{
                    activityCell.activityView.alpha = 0.0
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.20 * Double(index)) {
                        UIView.animate(withDuration: 0.2, animations: {
                            activityCell.activityView.alpha = 1.0
                            SystemSoundID.playFileNamed(fileName: "Pop", withExtenstion: "wav")
                            activityCell.activityView.pop(repeatCount: 1)
                        })
                    }
                    index += 1
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //self.randomSizeForActivityCell(indexPath: indexPath)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.propertiesForActivityCellAt(indexPath: indexPath).cellIdentifier, for: indexPath) as! ActivityNormalCell
    
        // Configure the cell
        let activityCellProperties = self.propertiesForActivityCellAt(indexPath: indexPath)
        cell.configure(index: indexPath.row, positionY: activityCellProperties.positionY, isCurrent: (indexPath.row == 3))
        cell.activityImageView.image = UIImage(named: "Activity\((indexPath.row % 4) + 1)")
        return cell
    }

    // MARK: Methods
    fileprivate func sizeFor(activityCellSize: ActivityCellSize) -> CGSize{
        switch activityCellSize {
        case .small:
            return CGSize(width: 195.0, height: 704.0)
        case .medium:
            return CGSize(width: 240.0, height: 704.0)
        case .large:
            return CGSize(width: 270.0, height: 704.0)
        }
    }
    
    fileprivate func propertiesForActivityCellAt(indexPath: IndexPath)->ActivityCellProperties{
        if let activityCellProperties = self.cellsProperties[indexPath]{
            return activityCellProperties
        }
        let randomSize = self.activityCellSizeRandom()
        let randomPosition = self.randomPositionForActivityCellAt(indexPath: indexPath)
        let activityCellProperties = ActivityCellProperties(activityCellSize: randomSize, positionY: randomPosition)
        self.cellsProperties[indexPath] = activityCellProperties
        return activityCellProperties
    }
    
    private func activityCellSizeRandom() -> ActivityCellSize{
        let randomNumer = Int.randomNumber(min: 1, max: 3)
        var activityCellSize: ActivityCellSize = ActivityCellSize.large
        switch randomNumer {
        case 1:
            activityCellSize = ActivityCellSize.small
            break
        case 2:
            activityCellSize = ActivityCellSize.medium
            break
        default:
            activityCellSize = ActivityCellSize.large
            break
        }
        return activityCellSize
    }
    
    private func randomPositionForActivityCellAt(indexPath: IndexPath) -> CGFloat{
        
        let cellPosition = (self.randomPosition.rawValue + indexPath.row) % 2 == 0 && self.randomPosition == ActivityCellPosition.top ? ActivityCellPosition.bottom : ActivityCellPosition.top
        let positionYFactor = self.collectionView.frame.height / 2.0
        var min = 0
        var max = 1
        switch cellPosition {
        case .top:
            min = 10
            max = Int(positionYFactor)
            break
        case .bottom:
            min = Int(positionYFactor + 10)
            max = Int(self.collectionView.frame.height )
            break
        }
        
        return CGFloat(Int.randomNumber(min: min, max: max))
    }

}

// MARK: - CollectionView Layout
extension KNCosmosViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let activityCellSize = self.propertiesForActivityCellAt(indexPath: indexPath).activityCellSize
        return self.sizeFor(activityCellSize: activityCellSize)
    }
}

import AudioToolbox

extension SystemSoundID {
    static func playFileNamed(fileName: String, withExtenstion fileExtension: String) {
        var sound: SystemSoundID = 0
        if let soundURL = Bundle.main.url(forResource: fileName, withExtension: fileExtension) {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &sound)
            AudioServicesPlaySystemSound(sound)
        }
    }
}
