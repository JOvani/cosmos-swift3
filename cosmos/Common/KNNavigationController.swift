//
//  KNNavigationController.swift
//  cosmos
//
//  Created by Jovani Luna on 22/02/17.
//  Copyright © 2017 knotion. All rights reserved.
//

import UIKit

class KNNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = UIColor(red: 39.0/255.0, green: 51.0/255.0, blue: 63.0/255.0, alpha: 1.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
