//
//  KNClassSelectorView.swift
//  cosmos
//
//  Created by Jovani Luna on 21/02/17.
//  Copyright © 2017 knotion. All rights reserved.
//

import UIKit

class KNClassSelectorView: UIView {

    @IBOutlet weak var leftBackgroundView: UIView!
    
    @IBOutlet weak var leftRoundedView: JFStylishView!
    
    @IBOutlet weak var mainView: JFStylishView!
    
    @IBOutlet weak var rightBackgroundView: UIView!
    
    @IBOutlet weak var rightRoundedView: JFStylishView!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    private var isExpanded: Bool = false{
        didSet{
            self.rotateArrow()
        }
    }
    
    override func awakeFromNib() {
        self.configure()
    }
    
    private func configure(){
        if self.backgroundColor != nil{
            if self.leftRoundedView != nil{
                self.leftRoundedView.backgroundColor = self.backgroundColor
            }
            if self.rightRoundedView != nil{
                self.rightRoundedView.backgroundColor = self.backgroundColor
            }
        }
        
        if self.color != nil{
            if self.mainView != nil{
            self.mainView.backgroundColor = self.color
            }
            
            if self.leftBackgroundView != nil{
                self.leftBackgroundView.backgroundColor = self.color
            }
            
            if self.rightBackgroundView != nil{
                self.rightBackgroundView.backgroundColor = self.color
            }
        }
    }
    
    override var backgroundColor: UIColor? {
        didSet{
            self.configure()
        }
    }
    
    var color: UIColor?{
        didSet{
            self.configure()
        }
    }
    
    @IBAction func tabGestureRecognizer(_ sender: Any) {
        self.isExpanded = !self.isExpanded
        
    }
    
    private func rotateArrow(){
        if let arrowImageView = self.arrowImageView{
            arrowImageView.rotate(toValue: !self.isExpanded ? 0.0 : CGFloat(M_PI_2))
        }
    }
}
