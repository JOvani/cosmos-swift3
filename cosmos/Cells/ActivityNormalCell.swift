//
//  ActivityNormalCell.swift
//  cosmos
//
//  Created by Jovani Luna on 22/02/17.
//  Copyright © 2017 knotion. All rights reserved.
//

import UIKit
import IBAnimatable

public enum ActivityCellPosition: Int{
    case top = 0
    case bottom = 1
}

class ActivityNormalCell: UICollectionViewCell {
    @IBOutlet weak var activityView: AnimatableView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityImageView: AnimatableImageView!
    
    override func awakeFromNib() {
        if let activityView = self.activityView{
            if activityView.motionEffects.count == 0{
                activityView.applyMotionEffect(magnitude: 40)
            }
        }
        
        if let activityImageView = self.activityImageView{
            if activityImageView.motionEffects.count == 0{
                activityImageView.applyMotionEffect(magnitude: -20)
            }
        }
    }
    
    func configure(index: Int, positionY: CGFloat, isCurrent: Bool){

        if let topConstraint = self.topConstraint{
            if positionY + self.activityView.frame.height > self.frame.height{
                topConstraint.constant = positionY - self.activityView.frame.height - 10
            }
            else{
                topConstraint.constant = positionY
            }
        }
        if isCurrent{
            
            UIView.animate(withDuration: 1.5, delay: 0.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
                self.activityView.layer.transform = CATransform3DMakeScale(1.1, 1.1, 1.1)
            }, completion: nil)
            
            //self.activityView.pop(repeatCount: 9999)
            //self.addPulseTo(view: self.activityView)
        }
        
    }
    
    private func addPulseTo(view: UIView){

        let pulseLayer = PulseLayer()
        pulseLayer.position = view.center
        pulseLayer.alphaFromValue = 1.0
        pulseLayer.keyTimeForHalfOpacity = 1.0
        pulseLayer.radius = view.frame.width
        pulseLayer.count = 2
        view.superview!.layer.insertSublayer(pulseLayer, below: view.layer)
        //view.layer.insertSublayer(pulseLayer, below: self.activityImageView.layer)
        pulseLayer.animate()

    }
    
}
