//: Playground - noun: a place where people can play
import Foundation
import UIKit
import XCPlayground

/*
var index = 1
print("\(index % 3) - \(index % 2)")
index = 2
print("\(index % 3) - \(index % 2)")
index = 3
print("\(index % 3) - \(index % 2)")
index = 4
print("\(index % 3) - \(index % 2)")
index = 5
print("\(index % 3) - \(index % 2)")
index = 6
print("\(index % 3) - \(index % 2)")
index = 7
print("\(index % 3) - \(index % 2)")
index = 8
print("\(index % 3) - \(index % 2)")
index = 9
print("\(index % 3) - \(index % 2)")
index = 10
print("\(index % 3) - \(index % 2)")
index = 11
print("\(index % 3) - \(index % 2)")
*/
0 % 4
1 % 4
2 % 4
3 % 4
4 % 4
5 % 4
6 % 4
7 % 4
8 % 4
9 % 4
10 % 4
11 % 4
12 % 4


/*
var view = UIView(frame: CGRect(x: 0, y: 0, width: 600, height: 600))
view.backgroundColor = UIColor.yellow

// create a new path
let path = UIBezierPath()

//Add top line
path.move(to: CGPoint(x: 2, y: 2))
path.addLine(to: CGPoint(x: 200, y: 2))

//Add curva top-left
path.addCurve(to: CGPoint(x: 200, y: 2), // ending point
    controlPoint1: CGPoint(x: 206, y: 6),
    controlPoint2: CGPoint(x: 206, y: 2))

path.addLine(to: CGPoint(x: 206, y: 200))



/*
// starting point for the path (bottom left)
path.move(to: CGPoint(x: 2, y: 26))

// *********************
// ***** Left side *****
// *********************

// segment 1: line
path.addLine(to: CGPoint(x: 2, y: 15))

// segment 2: curve
path.addCurve(to: CGPoint(x: 0, y: 12), // ending point
    controlPoint1: CGPoint(x: -5, y: 12),
    controlPoint2: CGPoint(x: 0, y: 14))

// segment 3: line
path.addLine(to: CGPoint(x: 0, y: 2))

// *********************
// ****** Top side *****
// *********************

// segment 4: arc
path.addArc(withCenter: CGPoint(x: 2, y: 2), // center point of circle
    radius: 2, // this will make it meet our path line
    startAngle: CGFloat(M_PI), // π radians = 180 degrees = straight left
    endAngle: CGFloat(3*M_PI_2), // 3π/2 radians = 270 degrees = straight up
    clockwise: true) // startAngle to endAngle goes in a clockwise direction

// segment 5: line
path.addLine(to: CGPoint(x: 8, y: 0))

// segment 6: arc
path.addArc(withCenter: CGPoint(x: 8, y: 2),
            radius: 2,
            startAngle: CGFloat(3*M_PI_2), // straight up
    endAngle: CGFloat(0), // 0 radians = straight right
    clockwise: true)

// *********************
// ***** Right side ****
// *********************

// segment 7: line
path.addLine(to: CGPoint(x: 10, y: 12))

// segment 8: curve
path.addCurve(to: CGPoint(x: 8, y: 15), // ending point
    controlPoint1: CGPoint(x: 10, y: 14),
    controlPoint2: CGPoint(x: 8, y: 14))

// segment 9: line
path.addLine(to: CGPoint(x: 8, y: 26))

// *********************
// **** Bottom side ****
// *********************
*/
// segment 10: line
path.close() // draws the final line to close the path

//path.apply(CGAffineTransform(translationX: 10, y: 10))

// Create a CAShapeLayer
let shapeLayer = CAShapeLayer()
// apply other properties related to the path
shapeLayer.strokeColor = UIColor.blue.cgColor
shapeLayer.fillColor = UIColor.white.cgColor
shapeLayer.lineWidth = 1.0
shapeLayer.position = CGPoint(x: 10, y: 10)

shapeLayer.path = path.cgPath



view.layer.addSublayer(shapeLayer)



XCPShowView(identifier: "Container View", view: view)
 
 */
