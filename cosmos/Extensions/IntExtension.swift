//
//  IntExtension.swift
//  cosmos
//
//  Created by Jovani Luna on 23/02/17.
//  Copyright © 2017 knotion. All rights reserved.
//

import UIKit

extension Int {
    public static func randomNumber(min: Int, max: Int) -> Int {
        
        return Int(arc4random_uniform(UInt32(max - min))) + min
    }
}

