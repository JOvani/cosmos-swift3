//
//  UIViewExtension.swift
//  cosmos
//
//  Created by Jovani Luna on 21/02/17.
//  Copyright © 2017 knotion. All rights reserved.
//

import UIKit

extension UIView {
    /// Returns a path for a rectangle with rounded corners
    public class func newPathForRoundedRect(_ rect:CGRect, radiusTL radTL:CGFloat, radiusTR radTR:CGFloat, radiusBL radBL:CGFloat, radiusBR radBR:CGFloat, edges:UIRectEdge = .all) -> CGPath
    {
        let retPath = CGMutablePath()
        
        // Convenience
        let rectL = rect.origin.x
        let rectR = rect.origin.x+rect.size.width
        let rectT = rect.origin.y
        let rectB = rect.origin.y+rect.size.height
        
        /*
         P2  P3
         P1      P4
         P8      P5
         P7  P6
         
         1.5π
         *
         π *   * 0
         *
         0.5π
         
         AT
         *
         AL *   * AR
         *
         AB
         */
        
        // Starting from the top left arc, move clockwise
        let p1 = CGPoint(x: rectL       , y: rectT+radTL)
        let p2 = CGPoint(x: rectL+radTL , y: rectT)
        let p3 = CGPoint(x: rectR-radTR , y: rectT)
        let p4 = CGPoint(x: rectR       , y: rectT+radTR)
        let p5 = CGPoint(x: rectR       , y: rectB-radBR)
        let p6 = CGPoint(x: rectR-radBR , y: rectB)
        let p7 = CGPoint(x: rectL+radBL , y: rectB)
        let p8 = CGPoint(x: rectL       , y: rectB-radBL)
        
        let c1 = CGPoint(x: rect.origin.x                   , y: rect.origin.y)
        let c2 = CGPoint(x: rect.origin.x+rect.size.width   , y: rect.origin.y)
        let c3 = CGPoint(x: rect.origin.x+rect.size.width   , y: rect.origin.y+rect.size.height)
        let c4 = CGPoint(x: rect.origin.x                   , y: rect.origin.y+rect.size.height)
        
        /*
         //let π = M_PI // The shortcut for π is Alt+P
         let aT : CGFloat = CGFloat(M_PI*1.5)
         let aR : CGFloat = 0
         let aB : CGFloat = CGFloat(M_PI_2)
         let aL : CGFloat = CGFloat(M_PI)
         let cTL = CGPoint(x: rect.origin.x                 + radTL/2, y: rect.origin.y                  + radTL/2)
         let cTR = CGPoint(x: rect.origin.x+rect.size.width - radTR/2, y: rect.origin.y                  + radTR/2)
         let cBL = CGPoint(x: rect.origin.x                 + radBL/2, y: rect.origin.y+rect.size.height - radBL/2)
         let cBR = CGPoint(x: rect.origin.x+rect.size.width - radBR/2, y: rect.origin.y+rect.size.height - radBR/2)
         */
        
        if(edges.contains(.all) || (edges.contains(.left) && edges.contains(.right) && edges.contains(.top) && edges.contains(.bottom)))
        {
            retPath.move(to: p1)
            retPath.addArc(tangent1End: c1, tangent2End: p2, radius: radTL)
            retPath.addLine(to: p3)
            retPath.addArc(tangent1End: c2, tangent2End: p4, radius: radTR)
            retPath.addLine(to: p5)
            retPath.addArc(tangent1End: c3, tangent2End: p6, radius: radBR)
            retPath.addLine(to: p7)
            retPath.addArc(tangent1End: c4, tangent2End: p8, radius: radBL)
            retPath.addLine(to: p1)
            
            retPath.closeSubpath()
            return retPath
        }
        
        if(edges.contains(.top))
        {
            retPath.move(to: p1)
            retPath.addArc(tangent1End: c1, tangent2End: p2, radius: radTL)
            retPath.addLine(to: p3)
            retPath.addArc(tangent1End: c2, tangent2End: p4, radius: radTR)
        }
        
        if(edges.contains(.right))
        {
            retPath.move(to: p3)
            retPath.addArc(tangent1End: c2, tangent2End: p4, radius: radTR)
            retPath.addLine(to: p5)
            retPath.addArc(tangent1End: c3, tangent2End: p6, radius: radBR)
        }
        
        if(edges.contains(.bottom))
        {
            retPath.move(to: p5)
            retPath.addArc(tangent1End: c3, tangent2End: p6, radius: radBR)
            retPath.addLine(to: p7)
            retPath.addArc(tangent1End: c4, tangent2End: p8, radius: radBL)
        }
        
        if(edges.contains(.left))
        {
            retPath.move(to: p7)
            retPath.addArc(tangent1End: c4, tangent2End: p8, radius: radBL)
            retPath.addLine(to: p1)
            retPath.addArc(tangent1End: c1, tangent2End: p2, radius: radTL)
        }
        
        return retPath
    }
}

extension UIView{
    public class func fromNib(nibNameOrNil: String? = nil) -> Self {
        return fromNib(nibNameOrNil: nibNameOrNil, type: self)
    }
    
    public class func fromNib<T : UIView>(nibNameOrNil: String? = nil, type: T.Type) -> T {
        let v: T? = fromNib(nibNameOrNil: nibNameOrNil, type: T.self)
        return v!
    }
    
    public class func fromNib<T : UIView>(nibNameOrNil: String? = nil, type: T.Type) -> T? {
        var view: T?
        let name: String
        if let nibName = nibNameOrNil {
            name = nibName
        } else {
            // Most nibs are demangled by practice, if not, just declare string explicitly
            name = nibName
        }
        guard let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil) else{ return nil }
        for v in nibViews {
            if let tog = v as? T {
                view = tog
            }
        }
        return view
    }
    
    public class var nibName: String {
        let name = "\(self)".components(separatedBy: ".").first ?? ""
        return name
    }
    public class var nib: UINib? {
        if let _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
            return UINib(nibName: nibName, bundle: nil)
        } else {
            return nil
        }
    }
}

extension UIView {
    
    func rotate(toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        
        self.layer.add(animation, forKey: nil)
    }
    
    func applyMotionEffect(magnitude: CGFloat){
        let xMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = -magnitude
        xMotion.maximumRelativeValue = magnitude
        
        let yMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = -magnitude
        yMotion.maximumRelativeValue = magnitude
        
        let motionEffectGroup = UIMotionEffectGroup()
        motionEffectGroup.motionEffects = [xMotion, yMotion]
        
        self.addMotionEffect(motionEffectGroup)
    }
    
}
