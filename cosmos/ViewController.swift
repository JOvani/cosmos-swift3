//
//  ViewController.swift
//  cosmos
//
//  Created by Jovani Luna on 21/02/17.
//  Copyright © 2017 knotion. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var backgroundImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let classSelectorView = KNClassSelectorView.fromNib()
        var frameClassSelector = classSelectorView.frame
        frameClassSelector.size.height = self.navigationController!.navigationBar.frame.height
        classSelectorView.frame = frameClassSelector
        let color = self.navigationController?.navigationBar.barTintColor ?? UIColor.white
        classSelectorView.backgroundColor = color
        classSelectorView.color = UIColor(red: 62.0/255.0, green: 110.0/255.0, blue: 134.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBottomBorderColor(color: classSelectorView.color!, height: 4.0)
        self.navigationItem.titleView = classSelectorView
        
        if let backgroundImageView = self.backgroundImageView{
            backgroundImageView.applyMotionEffect(magnitude: -25)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

